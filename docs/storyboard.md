#Storyboard
![Warmer](hyperlink)
![Reframe](hyperlink)
![Emotional Impact](hyperlink)
![Rational Drowning](hyperlink)
![A New Way](hyperlink)
![Our Solution](hyperlink)

##Variables
- Audience: *CTOs and CIOs attending a conference keynote*
- Motif: *Finance*
- Tone:  *Authoritative*
- Key Insights: *Importance of collaboration, innovation, and productivity to Platform Engineering*
- Goal of Warmer:  *Concerned about Engineering maturity*
- Current Belief:  *Platform Engineering is just another IT buzzword*
- New Perspective/Idea:  *unlock business advantage through technology by increasing productivity through collaboration and innovation*
- Objective of Reframe:
- Topic or Issue:
- Key Data Points & Insights:
- Desired Realization:
- Desired Emotional Response:
- Real-life Consequences:
- Background information:
- Current Belief or Practice:  *Community, collaboration, and technology*
- Desired Outcome:  *Transformative potential in reshaping business processes through InnerSource*
- Unique Value Proposition:  *If we build it, they won't come.  If they build it, they will love it!*
